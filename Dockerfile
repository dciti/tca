FROM steve353/centos:7.4.1708
#FROM dockerreg.training.local:5000/centos:latest
MAINTAINER Neueda and TPS
LABEL Version="1.0.0"
LABEL Description="Docker container to run Martins trade app"
EXPOSE 8080
EXPOSE 7036
EXPOSE 80
ENV APPVERSION=1.0.0
ENV INJECTOR=1.0.0
ENV MYSQL=
ENV ACTIVEMQ=
ENV ELK=
RUN yum -y install wget
RUN wget -nv 'https://www.dropbox.com/s/lqqp8zjc1ibmk8e/jdk-8u131-linux-x64.rpm?dl=0' -O /tmp/jdk-8u131-linux-x64.rpm
RUN wget 'https://dev.mysql.com/get/Downloads/Connector-J/mysql-connector-java-5.1.42.tar.gz' -O /tmp/mysql-connector-java-5.1.42.tar.gz
RUN yum -y install /tmp/jdk-8u131-linux-x64.rpm
RUN [ ! -d /app ] && mkdir -p /app/log
RUN chmod -R 777 /app
RUN cd /app && tar xvf /tmp/mysql-connector-java-5.1.42.tar.gz && mv mysql-connector-java-5.1.42/mysql-connector* /app
COPY target/trade-app-1.0.0-exec.jar /app/
COPY application.properties /app/
COPY application-injector.properties /app/
COPY injector-1.0.0.jar /app/
COPY start.sh /app/
RUN wget https://www.dropbox.com/s/0l3wbswee5ydc2z/geneos-netprobe-4.1.0.linux-x64.tar.gz?dl=0 -O /tmp/geneos-netprobe-4.1.0.linux-x64.tar.gz
RUN cd /app && tar xvf /tmp/geneos-netprobe-4.1.0.linux-x64.tar.gz
RUN wget https://artifacts.elastic.co/downloads/logstash/logstash-6.2.4.tar.gz -O /tmp/logstash.tgz
RUN cd /app && tar xvf /tmp/logstash.tgz
RUN mkdir /app/logstash-6.2.4/conf
COPY logstash/conf/* /app/logstash-6.2.4/conf/
RUN chmod 755 /app/start.sh
ADD src /app/src
VOLUME ["/app/log"]
ENTRYPOINT ["/app/start.sh"]

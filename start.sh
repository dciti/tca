#!/bin/bash

export VERSION=$APPVERSION
export INJECTORVERSION=$INJECTOR

# Configure DB and ACTIVEMQ servers
if [[ -n $MYSQL ]]
then
	if ! grep mysql.server /etc/hosts >/dev/null 2>&1
	then
		echo "$MYSQL	mysql.server" >>/etc/hosts
	else
		sed -i "s/^.*mysql\.server.*/$MYSQL	mysql.server/" /etc/hosts
	fi
fi

if [[ -n $ACTIVEMQ ]]
then
	if ! grep activemq.server /etc/hosts
	then
		echo "$ACTIVEMQ	activemq.server" >>/etc/hosts
	else
		sed -i "s/^.*activemq\.server.*/$ACTIVEMQ	activemq.server/" /etc/hosts
	fi
fi

if [[ -n $ELK ]]
then
	if ! grep elk.training.local /etc/hosts
	then
		echo "$ELK	elk.training.local" >>/etc/hosts
	else
		sed -i "s/^.*elk\.training\.local.*/$ELK	elk.training.local/" /etc/hosts
	fi
fi

if [[ -d /app/netprobe ]]
then
	# Start netprobe
	export LOG_FILENAME=/app/log/netprobe.log
	/app/netprobe/netprobe.linux_64 -port 7036 &
fi

if [[ -d /app/logstash-6.2.4 ]] && grep 'elk\.training\.local' /etc/hosts >/dev/null 2>&1
then
	# Start logstash
	/app/logstash-6.2.4/bin/logstash -f /app/logstash-6.2.4/conf -l /app/log &
fi

cd /app

/usr/bin/java -cp injector-${INJECTORVERSION}.jar -Dspring.config.location=application-injector.properties -Dloader.main=com.neueda.trade.injector.Injector org.springframework.boot.loader.PropertiesLauncher >log/injector.log 2>&1 &
java -Dspring.config.location=application.properties -jar trade-app-${VERSION}-exec.jar >log/trade-app.log 2>&1

# Keep container alive
while :
do
	sleep 60
done
